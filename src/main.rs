extern crate tradfrs;
extern crate dotenv;

use self::tradfrs::home::Home;
use std::thread;
use std::time::Duration;
use std::env;

fn main(){
    dotenv::dotenv().ok();
    let mut home = Home::new(env::var("HOST_IP").unwrap(),env::var("SECRET_KEY").unwrap()).unwrap();
    let mut device = &mut home.devices()[1];
    let mut i = 0;
    let mut rise = true;
    loop{
        if rise{
            i += 1;
        }else{
            i -= 1;
        }
        if i == 0{
            rise = true;
        }
        if i == 255{
            rise = false;
        }

        device.light_control
            .as_mut()
            .unwrap()
            .set_dimmer(i as u8)
            .unwrap();
        thread::sleep(Duration::from_millis(250));
    }
}
