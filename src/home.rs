
use std::rc::Rc;

use super::api::Api;
use super::{constants,Error};
use super::serde_json::Value;

#[derive(Debug)]
pub struct Home{
    api: Rc<Api>,
    //gateway: Gateway,
    devices: Vec<Device>,
}

unsafe impl Send for Home{}

impl Home{
    pub fn new(ip: String, key: String) -> Result<Self,Error>{
        let api = Rc::new(Api::new(ip,key)?);
        let device_id: Vec<i64> = api.request_json("get",&[constants::ROOT_DEVICES],None)?.unwrap();
        let mut devices = Vec::with_capacity(device_id.len());
        for id in device_id{
            devices.push(Device::new(api.clone(),id)?);
        }
        Ok(Home{
            api: api,
            devices: devices,
        })
    }

    pub fn reload(&mut self) -> Result<(),Error>{
        let device_id: Vec<i64> = self.api.request_json("get",&[constants::ROOT_DEVICES],None)?.unwrap();
        let mut devices = Vec::with_capacity(device_id.len());
        for id in device_id{
            devices.push(Device::new(self.api.clone(),id)?);
        }
        *self = Home{
            api: self.api.clone(),
            devices: devices,
        };
        Ok(())
    }

    pub fn devices(&self) -> &[Device]{
        &self.devices
    }

    pub fn devices_mut(&mut self) -> &mut [Device]{
        &mut self.devices
    }
}

#[derive(Debug)]
pub struct Device{
    api: Rc<Api>,
    id: i64,
    name: String,
    device_type: String,
    light_control: Option<Lighting>,
}

impl Device{
    fn new(api: Rc<Api>,id: i64) -> Result<Self,Error>{
        let val: Value = api.request_json("get",&[constants::ROOT_DEVICES, &id.to_string()],None)?.unwrap();
        let obj = val.as_object().unwrap();
        let name = obj.get(constants::ATTR_NAME)
            .unwrap().as_str()
            .unwrap().clone();

        let device_type = obj.get(constants::ATTR_DEVICE_INFO).unwrap()
            .as_object().unwrap()
            .get("1").unwrap()
            .as_str().unwrap();

        let light_control = obj.get(constants::ATTR_LIGHT_CONTROL)
            .map(|e| Lighting::new(api.clone(),id,e));
        Ok(Device{
            api: api,
            id: id,
            device_type: device_type.to_string(),
            name: name.to_string(),
            light_control: light_control,
        })
    }
    
    pub fn id(&self) -> i64{
        self.id
    }

    pub fn name(&self) -> &str{
        &self.name
    }

    pub fn ty(&self) -> &str{
        &self.device_type
    }

    pub fn control(&self) -> Option<&Lighting>{
        self.light_control.as_ref()
    }

    pub fn control_mut(&mut self) -> Option<&mut Lighting>{
        self.light_control.as_mut()
    }
}

#[derive(Debug,Clone)]
pub enum Color{
    Hex(String),
    Values(i64,i64),
}

#[derive(Debug)]
pub struct Lighting{
    api: Rc<Api>,
    id: i64,
    color: Color,
    state: i64,
    dimmer: u8,
}

impl Lighting{
    fn new(api: Rc<Api>,id: i64,val: &Value) -> Self{
        let obj = val.as_array().unwrap()[0]
            .as_object().unwrap();
        let color = obj.get(constants::ATTR_LIGHT_COLOR).unwrap()
            .as_str().unwrap();
        let state = obj.get(constants::ATTR_LIGHT_STATE).unwrap()
            .as_i64().unwrap();
        let dimmer = obj.get(constants::ATTR_LIGHT_DIMMER).unwrap()
            .as_i64().unwrap() as u8;
        Lighting{
            api: api,
            id: id, 
            color: Color::Hex(color.to_string()),
            state: state,
            dimmer: dimmer,
        }
    }

    pub fn set_dimmer(&mut self,value: u8) -> Result<(),Error>{
        let body = json!({
            constants::ATTR_LIGHT_CONTROL: [
                { constants::ATTR_LIGHT_DIMMER: value },
            ]
        });
        let body_str = body.to_string();
        self.api.request("put"
                         ,&[constants::ROOT_DEVICES,&self.id.to_string()]
                         ,Some(&body_str))?;
        self.dimmer = value;
        Ok(())
    }

    pub fn set_state(&mut self,state: i64) -> Result<(), Error>{
        let body = json!({
            constants::ATTR_LIGHT_CONTROL:
                [ { constants::ATTR_LIGHT_STATE: state }, ]
        });
        let body_str = body.to_string();
        self.api.request("put"
                        , &[constants::ROOT_DEVICES,&self.id.to_string()]
                        , Some(&body_str))?;
        self.state = state;
        Ok(())
    }

    pub fn set_color(&mut self,color: Color) -> Result<(), Error>{
        match color.clone(){
            Color::Hex(x) => {
                let body = json!({
                    constants::ATTR_LIGHT_CONTROL:
                        [ { constants::ATTR_LIGHT_COLOR: x }, ]
                }).to_string();
                self.api.request( "put"
                                , &[constants::ROOT_DEVICES, &self.id.to_string()]
                                , Some(&body))?;
            },
            Color::Values(x,y) => {
                let body = json!({
                    constants::ATTR_LIGHT_CONTROL:
                        [ { 
                            constants::ATTR_LIGHT_COLOR_X: x,
                            constants::ATTR_LIGHT_COLOR_X: y,
                        }, ]
                }).to_string();
                self.api.request( "put"
                                , &[constants::ROOT_DEVICES, &self.id.to_string()]
                                , Some(&body))?;
            },
        }
        self.color = color;
        Ok(())
    }

    pub fn dimmer(&self) -> u8{
        self.dimmer
    }

    pub fn state(&self) -> i64{
        self.state
    }

    pub fn color(&self) -> Color{
        self.color.clone()
    }
}

#[cfg(test)]
mod test{
    extern crate dotenv;
    use super::*;

    use self::dotenv::dotenv;
    use std::env;

    #[test]
    fn test_home(){
        dotenv().ok();

        let home = Home::new(env::var("HOST_IP").unwrap(),env::var("SECRET_KEY").unwrap()).unwrap();
        println!("{:#?}",home);
    }
}
