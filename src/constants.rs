pub static ROOT_DEVICES: &'static str = "15001";
pub static ROOT_GROUPS: &'static str = "15004";
pub static ROOT_MOODS: &'static str = "15005";
pub static PATH_GATEWAY_INFO: [&'static str;2]  = ["15011", "15012"];

pub static ATTR_APPLICATION_TYPE: &'static str = "5750";
pub static ATTR_DEVICE_INFO: &'static str = "3";
pub static ATTR_NAME: &'static str = "9001";
pub static ATTR_CREATED_AT: &'static str = "9002";
pub static ATTR_ID: &'static str = "9003";
pub static ATTR_REACHABLE_STATE : &'static str= "9019";
pub static ATTR_LAST_SEEN: &'static str = "9020";
pub static ATTR_LIGHT_CONTROL: &'static str = "3311";

pub static ATTR_NTP: &'static str = "9023";
pub static ATTR_FIRMWARE_VERSION: &'static str = "9029";
pub static ATTR_CURRENT_TIME_UNIX: &'static str = "9059";
pub static ATTR_CURRENT_TIME_ISO8601: &'static str = "9060";
pub static ATTR_FIRST_SETUP: &'static str = "9069";  // ??? unix epoch value when gateway first setup
pub static ATTR_GATEWAY_ID: &'static str = "9081";  // ??? id of the gateway

pub static ATTR_LIGHT_STATE: &'static str = "5850";  // 0 / 1
pub static ATTR_LIGHT_DIMMER: &'static str = "5851";  // Dimmer, not following spec: 0..255
pub static ATTR_LIGHT_COLOR: &'static str = "5706";  // string representing a value in some color space
pub static ATTR_LIGHT_COLOR_X: &'static str = "5709";
pub static ATTR_LIGHT_COLOR_Y: &'static str = "5710";

pub static CLIENT_ERROR_PREFIX: &'static str = "4.";
pub static SERVER_ERROR_PREFIX: &'static str = "5.";
