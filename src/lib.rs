#![allow(dead_code)]
#[macro_use]
extern crate serde_json;
extern crate serde;

mod constants;
mod api;
pub mod home;

use std::io::Error as IoError;
use self::serde_json::error::Error as SeError;

pub use self::home::Home;

#[derive(Debug)]
pub enum Error{
    InvalidIp,
    Timeout,
    IoError(IoError),
    FailedCommand,
    DecodeError(SeError),
    ClientError,
    ServerError,
}

impl From<IoError> for Error{
    fn from(e: IoError) -> Self{
        Error::IoError(e)
    }
}

impl From<SeError> for Error{
    fn from(e: SeError) -> Self{
        Error::DecodeError(e)
    }
}

