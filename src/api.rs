
use super::serde::de::DeserializeOwned;
use super::serde_json;
use std::io::Cursor;

use std::process::{Stdio,Command};
use std::time::{Instant,Duration};
use std::thread;

use std::io::{Write};

use super::Error;
use super::constants;

#[derive(Debug)]
pub struct Api{
    host: String,
    security_code: String,
}

impl Api{
    pub fn new(ip: String,sec_code: String) -> Result<Self,Error>{
        let api = Api{
            host: ip,
            security_code: sec_code,
        };
        api.request("get",&["status"],None)?;
        Ok(api)
    }

    pub fn request(&self,method: &str,path: &[&str], data: Option<&str>) -> Result<Option<String>,Error>{

        let mut p = String::new();
        if path.len() != 0{
            p.push_str(path[0]);
        }
        for part in path[1..].iter(){
            p.push_str("/");
            p.push_str(part);
        }

        let url = format!("coaps://{}:5684/{}",self.host,p);

        let thread = thread::current();
        let sec_code = self.security_code.clone();
        let method = method.to_string();
        let data = data.map(|e| e.to_string());
        let join = thread::spawn(move ||{
            let res = if let Some(x) = data{
                println!("Data: {}",x);
                let mut child = Command::new("coap-client")
                    .arg("-u")
                    .arg("Client_identity")
                    .arg("-k")
                    .arg(&sec_code)
                    .arg("-v")
                    .arg("0")
                    .arg("-m")
                    .arg(method)
                    .arg(url)
                    .arg("-f")
                    .arg("-")
                    .stdin(Stdio::piped())
                    .spawn()
                    .unwrap();
                child.stdin.as_mut().unwrap().write(x.as_bytes()).unwrap();
                child.wait_with_output().unwrap()
            }else{
                Command::new("coap-client")
                    .arg("-u")
                    .arg("Client_identity")
                    .arg("-k")
                    .arg(&sec_code)
                    .arg("-v")
                    .arg("0")
                    .arg("-m")
                    .arg(method)
                    .arg(url)
                    .output()
                    .unwrap()
            };
            thread.unpark();
            res
        });

        let timeout = Duration::from_secs(10);
        let begin = Instant::now();
        thread::park_timeout(timeout);

        let output = if begin.elapsed() < timeout{
            match join.join(){
                Ok(x) => {
                    x
                }
                Err(_) => {
                    return Err(Error::FailedCommand);
                }
            }
        }else{
            return Err(Error::Timeout);
        };


        let str_output = String::from_utf8(output.stdout).unwrap();
        let line = str_output.lines()
            .filter(|e| !e.starts_with("decrypt_verify"))
            .last()
            .map(|s| s.to_string());

        if line.as_ref()
            .map(|e| e.starts_with(constants::SERVER_ERROR_PREFIX))
            .unwrap_or(false){
            return Err(Error::ServerError);
        }

        if line.as_ref()
            .map(|e| e.starts_with(constants::CLIENT_ERROR_PREFIX))
            .unwrap_or(false){
            return Err(Error::ClientError);
        }

        Ok(line)
    }

    pub fn request_json<'a,D: DeserializeOwned>(&self,method: &str,path: &[&str], data: Option<&str>) -> Result<Option<D>,Error>{
        let output = self.request(method,path,data)?;
        match output {
            Some(x) => {
                let val: D = serde_json::from_reader(Cursor::new(&x))?;
                Ok(Some(val))
            },
            None => {
                Ok(None)
            }
        }
    }
}

#[cfg(test)]
mod test{
    extern crate dotenv;

    use self::dotenv::dotenv;
    use std::env;

    use super::*;
    fn test_api_creation(){
        dotenv().ok();

        Api::new(env::var("HOST_IP").unwrap(),env::var("SECRET_KEY").unwrap()).unwrap();
    }
}
